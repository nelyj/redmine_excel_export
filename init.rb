Redmine::Plugin.register :excel_export do
  name 'NovaRed - Excel Export plugin'
  author 'Nelson Jimenez'
  description 'Export project information with excel button'
  version '0.0.1'
  url 'https://bitbucket.org/nelyj/redmine_excel_export/src/master/'
  author_url 'https://github.com/nelyj'

  permission :download, { downloads: [:index] }, public: true
  menu :application_menu, :downloads, { controller: 'downloads', action: 'index' }, caption: 'Descargar Hitos', after: :announcements
end
