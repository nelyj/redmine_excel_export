class DownloadsController < ApplicationController
  def index
    data = []
    Project.where(status: 1).each do |project|
      project.issues.includes(:tracker).where(trackers: { name: 'Facturacion' }).each do |issue|
        entry = {}
        entry[:proyecto] = project.name
        entry[:tipo] = issue.tracker.name

        project.custom_values.each do |custom_value|
          custom_field_name = custom_value.custom_field.name
          value = custom_value.value
          entry[:ot] = value if custom_field_name === 'OT'
        end
        entry[:contrato_servicio] = project.name.split("//")[0]&.squish || ''

        # Jefe de proyecto
        pmo =  project.members.select {|m| m.member_roles.first.role.name == 'Pmo' }.first
        entry[:jefe_de_proyecto] = "#{pmo.user.firstname} #{pmo.user.lastname}" if pmo.present?

        entry[:cuenta] = project.name.split("//")[1]&.squish || ''

        entry[:descripcion] =  project.name.split("//")[2]&.squish
        entry[:hito] = ''

        custom_values = issue.custom_values.includes(:custom_field)

        monto_hito = custom_values.where(custom_fields: { name: 'Monto' })

        entry[:monto_del_hito] = monto_hito.size.zero? ? 0 : monto_hito.first.value

        divisa_actividad = custom_values.where(custom_fields: { name: 'Divisas Monto' })

        entry[:divisa_actividad] = divisa_actividad.size.zero? ? 'Sin divisa' : divisa_actividad.first.value

        facturado = custom_values.where(custom_fields: { name: 'Facturado' })

        entry[:facturado] = if facturado.size.zero?
                              'No'
                            elsif facturado.first.value.to_i.zero?
                              'No'
                            else
                              'Si'
                            end

        entry[:asunto] = issue.subject.squish

        I18n.locale = :es
        entry[:fecha_fin] = issue.due_date.present? ? I18n.l(issue.due_date, format: :long) : ''
        entry[:estado] = issue.status&.name
        entry[:fecha_modificacion] = I18n.l(issue.updated_on, format: :long)

        # Status
        #status_id = project.status
        #status_name = IssueStatus.find(status_id).name if status_id.present?
        #entry[:status] = status_name if status_id.present?

        # Resultor
        #resultores = project.members.select {|m| m.member_roles.first.role.name == 'Ingenierio' }

        #entry[:resultor] = ""

        #resultores.each_with_index do |val, index|
        #  entry[:resultor] += ", " if index > 0
        #  entry[:resultor] += "#{val.user.firstname} #{val.user.lastname}"
        #end


        entry[:name] = project.name
        #entry[:tiempo_utilizado] = project.time_entries.sum(&:hours)
        #entry[:tiempo_declarado] = project.issues.sum {|i| i.estimated_hours.to_f }
        #I18n.locale = :es
        #entry[:fecha] = I18n.l(project.created_on, format: :long)
        #entry[:mes] = I18n.l(project.created_on, format: "%B")

        data << entry if entry.present?
      end
    end
    @excel_data = data

    render "index.xlsx.axlsx"
    # render :json => data, status: 200
  end
end
